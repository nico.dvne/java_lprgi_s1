package decouverte.impl;

import decouverte.api.Demarrable;

public class VideoProjecteur implements Demarrable {
    private int puissanceAmpoule;
    private boolean enMarche;

    public VideoProjecteur(int puissanceAmpoule){
        this.puissanceAmpoule = puissanceAmpoule;
        this.enMarche = false;
    }

    public int getPuissanceAmpoule() {
        return puissanceAmpoule;
    }

    @Override
    public void on() {
        if(!enMarche){
            enMarche = true;
            System.out.println("Le projecteur demarre");
            return;
        }
        System.out.println("Le projecteur est deja demarée");

    }

    @Override
    public void off() {
        if(enMarche){
            enMarche = false;
            System.out.println("Le projecteur s'arrete");
            return;
        }
        System.out.println("Le projecteur est deja arretée");
    }

    @Override
    public String toString() {
        return "VideoProjecteur{" +
                "puissanceAmpoule=" + puissanceAmpoule +
                ", enMarche=" + enMarche +
                '}';
    }
}
