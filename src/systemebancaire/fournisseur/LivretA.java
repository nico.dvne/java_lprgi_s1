package systemebancaire.fournisseur;

public class LivretA extends CompteBancaire{

    private double tauxInteret; //Encodé de facon 0.xx

    public LivretA(String nom, double solde, double tauxInteret) {
        super(nom,solde);
        this.tauxInteret = tauxInteret;
    }

    public double getTauxInteret() {
        return this.tauxInteret;
    }

    public void verserInterer(){
        //interets = this.getSolde() * this.getTauxInteret();
        this.depot(this.getSolde() * this.getTauxInteret());
    }

    @Override
    public String toString() {
        return super.toString() + "{" +
                " tauxInteret=" + tauxInteret +
                " }";
    }
}
