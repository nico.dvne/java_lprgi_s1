package systemebancaire.fournisseur;

public class CompteBancaire {
    private double solde;
    private String nomTitulaire;
    private int numeroDeCompte;

    private static int compteurDeComptes = 0;

    public static final String NOM_PAR_DEFAUT = "Nom par défaut";

    private CompteBancaire(){
        numeroDeCompte = ++compteurDeComptes;
    }

    public CompteBancaire(String nom){
        this(nom,0.0);
    }

    /**
     *
     * @param nomTitulaire : Nom du titulaire du compte
     * @param sommeInitiale : Somme initiale déposée sur le compte
     */
    public CompteBancaire(String nomTitulaire, double sommeInitiale){
        this();
        setNomTitulaire(nomTitulaire);
        this.solde = (sommeInitiale<=0.0)?0.0:sommeInitiale;
    }

    public double GetSolde(){
        return this.solde;
    }

    public void depot(double somme)
    {
        if(somme<0.0) return;

        this.solde += somme;
    }

    public void depot(int somme){
        this.depot((double)somme);
    }

    public double retrait(double somme){
        if(somme<0.0 || somme > this.solde) return 0.0;

        this.solde -= somme;

        return somme;
    }

    @Override
    public String toString() {
        return "CompteBancaire{ " +
                "solde = " + solde +
                ", nomTitulaire=' " + nomTitulaire + '\'' +
                ", numeroDeCompte= " + numeroDeCompte +
                " }";
    }

    public double getSolde() {
        return solde;
    }

    public String getNomTitulaire() {
        return nomTitulaire;
    }

    public int getNumeroDeCompte() {
        return numeroDeCompte;
    }

    public void setSolde(double solde) {
        if(solde==0.0)return;
        if(solde<0.0)
            this.retrait(-solde);
        else
            this.depot(solde);
    }

    public void setNomTitulaire(String nomTitulaire) {
        this.nomTitulaire = ((nomTitulaire!=null)&&!nomTitulaire.isEmpty())?nomTitulaire:NOM_PAR_DEFAUT;
    }

    public void setNumeroDeCompte(int numeroDeCompte) {
        this.numeroDeCompte = numeroDeCompte;
    }
}
