package decouverte.impl;

import decouverte.api.Demarrable;

import java.awt.*;

public class Voiture implements Demarrable {

    private Color couleur;
    private boolean enMarche;

    public Voiture(Color couleur){
        this.couleur = couleur;

        this.enMarche = false;
    }

    public Voiture(){
        this(Color.BLUE);
    }

    public Color getCouleur(){
        return this.couleur;
    }

    @Override
    public void on() {
        if(!enMarche){
            enMarche = true;
            System.out.println("La voiture demarre");
            return;
        }
        System.out.println("La voiture est deja demarrée");

    }

    @Override
    public void off() {
        if(enMarche){
            enMarche = false;
            System.out.println("La voiture s'arrete");
            return;
        }
        System.out.println("La voiture est deja arretée");
    }

    @Override
    public String toString() {
        return "Voiture{" +
                "couleur=" + couleur +
                ", enMarche=" + enMarche +
                '}';
    }
}
