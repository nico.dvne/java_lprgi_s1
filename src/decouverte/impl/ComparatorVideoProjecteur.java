package decouverte.impl;

import java.util.Comparator;

public class ComparatorVideoProjecteur implements Comparator<VideoProjecteur> {
    @Override
    public int compare(VideoProjecteur o1, VideoProjecteur o2) {
        return o1.getPuissanceAmpoule() - o2.getPuissanceAmpoule();
    }
}
