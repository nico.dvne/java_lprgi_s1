package decouverte.utilisation;

import decouverte.api.Demarrable;
import decouverte.impl.VideoProjecteur;
import decouverte.impl.Voiture;

import java.awt.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        System.out.println("Debut de test");

        Voiture voiture = new Voiture();

        voiture.on();
        voiture.on();
        voiture.off();
        voiture.off();

        System.out.println(voiture);

        List<Demarrable> sontDemarrables;
        sontDemarrables = Arrays.asList(
                new Voiture(Color.RED),
                new Voiture(),
                new VideoProjecteur(500),
                new VideoProjecteur(13),
                new Voiture(Color.GREEN),
                new VideoProjecteur(6)
        );

        sontDemarrables.forEach(System.out::println);

        List<VideoProjecteur> desProjecteurs = new LinkedList<>();
        sontDemarrables.stream()
                .filter(d->{return d instanceof VideoProjecteur;})
                .forEach(vp->{desProjecteurs.add((VideoProjecteur) vp);});

        desProjecteurs.forEach(System.out::println);


        System.out.println("Fin de test");


    }
}
