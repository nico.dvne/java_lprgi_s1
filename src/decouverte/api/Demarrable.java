package decouverte.api;

public interface Demarrable {

    void on();
    void off();

}
