package systemebancaire.utilisateur;

import systemebancaire.fournisseur.CompteBancaire;

public class TestCompteBancaire {
    public static void main(String[] args) {
        CompteBancaire premierCompteBancaire;

        premierCompteBancaire = new CompteBancaire("Toto",100.);

        double reste = premierCompteBancaire.GetSolde();

        System.out.println("Il me reste : " + reste);

        System.out.println(premierCompteBancaire);
    }
}
